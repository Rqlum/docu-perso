Linux
=====

Historique
1.  Pour l'historique, référence : *Culture Numérique, Dominique Cardon, Presses de Sciences Po, 2019*.
2.  Notion de commun... Au début (années 70), flou artistique, tout se partage librement.
3.  En 1976, Bill Gates s'énerve (lettre à un groupe de bidouilleurs des débuts de l'informatique).
4.  À partir des années 80, MS-DOS équipe les PC d'IBM.
5.  Pendant ce temps-là, les développeurs utilisent couramment UNIX, développé par AT&T, jusqu'à sa commercialisation. Parler du développement en assembleur puis en C → portabilité, etc.
6.  Alors en 1983, Richard Stallman (qui fonde ensuite la Free Software Foundation) décide de créer GNU (GNU's Not Unix), un OS qui a les fonctionnalités d'UNIX mais qui est **libre (ouvert, gratuit, partageable)**. (Autres projet similaire : BSD, développé à Berkeley...)
7.  Dans les années 80, le développement d'Internet favorise, par son architecture décentralisée, la formation de communautés *numériques* (← mot anachronique) gérées par et pour leurs membres, sans dépendance ni contrôle par une organisation centrale (état, entreprise...). L'idée basique est de partager au maximum les connaissances et les algorithmes. Certaines personnes (dont Stallman) revendiquent l'importance de cette philosophie du libre (ouverture, gratuité, partage). Légère différence de philosophie avec open-source : libre suggère une idée politique. Dans les deux cas, l'ouverture permet une grande communauté d'utilisateurs/développeurs/testeurs, donc une grande stabilité.
8.  Les logiciels principaux du projet GNU sont développés, mais manque le noyau ! Dire ce qu'est le noyau (couche logicielle très bas-niveau qui fait le lien entre programmes et composants matériels de l'ordinateur).
9.  En 1991, Linus Torvalds (étudiant de 22 ans en Finlande) commence à développer un noyau pour son ordinateur personnel pour pallier au manque de noyau libre et à l'insuffisance du serveur (propriétaire) de la fac d'Helsinki. Collaboration puis en 1994 sort le noyau Linux 1.0, qui complémente les logiciels GNU pour former les distributions GNU/Linux dont on a parlé.
10. Différentes distributions existent mais Ubuntu (basée sur Debian et créée en 2004 par une entreprise, Canonical) s'impose récemment comme LA distribution GNU/Linux grand public. Ou plutôt LES distributions, car il existe Kubuntu, Xubuntu, Lubuntu, Edubuntu, etc.
11. En parallèle, on a Windows (Microsoft, Bill Gates) :
    -   Se répend chez le grand public en gros avec Windows 95
    -   Gros lobby, omniprésent, commercial
    -   Non Unix, non libre
12. On a aussi Mac (Apple, Steve Jobs) :
    -   Se place dans la généalogie Unix (donc inclus des programmes de base de même noms et fonctionnalités).Intéressant car en 1984 (création), pas Unix, et interface intuitive mais manque de fonctionnalités ; Unix depuis 2001.
    -   Principe = tout fonctionne très bien dans mon univers privé
        (incluant le matériel)
    -   Commercial, non libre
    -   S'impose en partie chez les académiques pour des raisons historiques, puis sur le marché des téléphones (Microsoft quasi inexistant), et dans l'audiovisuel pour leurs logiciels spécialisés

Aides des commandes
-------------------
1. **pwd** : affiche le chemin d'accès vers le répertoire où se situe l'utilisateur qui a entré la 
commande (affiche l'adresse de la fenêtre courante)
2. **cd** : sert à se placer dans un répertoire
**cd ..** : permet de remonter au répertoire (dossier) parent 
3. **mkdir** : crée un répertoire vide
4. **touch** : crée des fichiers vides
5. **ls** : affiche le contenu du répertoire
6. **cat fichier** : permet d'afficher le contenu de fichier
7. **echo bonjour > essai** : ajoute le texte bonjour dans le fichier essai
8. **head long_texte** : affiche les premières lignes (par défaut, les 10 premières lignes) du fichier 
texte long_texte
9. **mv test test_mv** : renomme le fichier test en test_mv


Git
===

Historique
----------
- 2002 : le scandale initial. Linus Torvalds décide d’utiliser BitKeeper, VCS décentralisé mais logiciel propriétaire pour le développement du noyau Linux.
- 2005 : ce qui devait arriver. BitMover annonce la fin de la version gratuite de BitKeeper.
Dans la foulée, Git est développé par Linus Torvalds pour remplacer BitKeeper dans le projet Linux.
- 2016 : Git domine. En 2016, Git est le VCS le plus populaire, utilisé par plus de 12 millions de personnes.

Aides des commandes
-------------------

Markdown ?
======

Petite aide :

-   Cf. le code pour voir comment faire une liste à puces.
-   Utiliser `1.` à la place des `-` pour faire une liste numérotée.
-   Un saut de ligne simple est ignoré, un double saut de ligne sépare
    deux paragraphes.
-   Pour formatter le texte :
    -   la syntaxe `` `de code...` `` permet d'écrire des petits bouts
        `de code...` (apostrophe inversée : `AltGr-7`).
    -   Des indentations cohérentes (au moins quatre) permettent de
        mettre plusieurs lignes de code en forme, pour montrer par
        exemple :
            
            $ git log --oneline
            13040e0 (HEAD -> master, origin/master) Ajoute mini-projet documentation
            190c664 Corrige typo exo2
            d66e9ec Ajoute mini-projet simuRPG

    -   `_texte_` met le _texte_ en italique.
    -   `**texte**` met le **texte** en gras.
-   Libre à vous de compléter, éventuellement !
